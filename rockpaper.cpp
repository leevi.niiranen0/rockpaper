#include <iostream>
#include <stdio.h>
#include <stdlib.h>

int main() {
    std::string rps[4] = {"Null", "Rock", "Paper", "Sciccors"};                     // Define Values
    int x = 0;
    int y = 0;
    srand (time(NULL));                                                             // Generate Random seed

    std::cout << rps[1] << " (1) / " << rps[2] << " (2) / " << rps[3] <<" (3)\n";   // Display values 
    std::cin >> x;
    std::cout << "You Chose : " << rps[x] <<"\n";

    y = rand() % 3 + 1;                                                             // Generate compters choice
    std::cout << "Computer Chose : " << rps[y] <<"\n";

    if ((x >= 4) || (x <= 0)) { 
	std::cout << "Please enter only 1, 2 or 3";
    }

//  x = player  y = Computer
//  1 = Rock    2 = Paper   3 = Sciccors
//  If x is one more than y then x loses, but if x is two more than y, x wins
//  If x is equal to y it is a tie
//  Otherwise the only option is that the player loses
//  For example:     x is 2 and y is 1  (1+1=2) is true and the player wins
//  Example 2:       x is 3 and y is 1  (1+1=3) is not true then (1+1=4) is not true then (3=1) is not true then the player loses     

    if ((y+1==x) || (y+1==4) && (x==1)) {                                           // Calculate if player wins
	std::cout << rps[x] << " Beats " << rps[y] << ". You Win!\n";
    } else if (x == y) {
	std::cout << "It is a Tie!\n";                                                  // Calculate if it is a tie
    } else {
	std::cout << rps[y] << " Beats " << rps[x] << ". You Lost!\n";                  // Else the player lost
    }
}
